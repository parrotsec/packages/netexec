# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/netexec/issues
# Bug-Submit: https://github.com/<user>/netexec/issues/new
# Changelog: https://github.com/<user>/netexec/blob/master/CHANGES
# Documentation: https://github.com/<user>/netexec/wiki
# Repository-Browse: https://github.com/<user>/netexec
# Repository: https://github.com/<user>/netexec.git
